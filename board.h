#ifndef BOARD_H
#define BOARD_H

#include <QtGui>
#include "piece.h"
#include "coord.h"

class QButtonGroup;
class QGridLayout;
class QWidget;

class board : public QWidget {
    Q_OBJECT

private:
   int iSize;
   bool moved;
   bool jumped;
   bool pieceJumped;
   bool infoSet;

   piece* movingPiece;
   QDialog * winnerWindow;
   QLabel * championLabel;
   QPushButton* pass;
   
   QIcon* redSquare;
   QIcon* blackSquare;
   QIcon* highlightedSquare;
   QIcon * redPieceIcon;
   QIcon * blackPieceIcon;

   piece* lastClicked; //Holds the piece data for the last button clicked
   std::vector<coord*> lastPath; //Holds list of possible locations the last piece can move to
   std::vector<coord*> jumpPath; //The path that pieces can jump to
   std::vector<coord*> testPath;
   std::vector<piece*> redPieces;   //Red pieces
   std::vector<piece*> blackPieces; //Black pieces

   void resetPaths();


public:
   QButtonGroup* squareButton; //All the  buttons for the 'board'
   QGridLayout* checkersGrid;   //The gridlayout for the board
   QVBoxLayout * mainLayer;
   QLabel * turnLabel;
   int numRed;
   int numBlack;

   void gameOver();
   bool checkGameOver();//returns true if a player cannot move or has no pieces left.
   QPushButton* accept;

   board(); //Constructer: sets up all pieces
   void resetPieces(); //Resets pieces and their respective locations
   void highlightSquares(); //Will highlight squares the squares a piece can move to.
   piece* getPiece(coord* location); //Returns the piece at specified location
   bool jumpable(piece* one, piece* two); //Will return true if piece one can jump piece two
   void movePiece(piece* mover, coord* destination); //Moves piece to destination
   void jumpPiece(piece* jumper, coord* destination); //Jumps 'jumped' -> kills the jumped piece

public slots:
   void getPath (int buttonID); //Will find and highlight all buttons that are a valid movement path if the button clicked was a piece
   // void getMultiPath (int buttonID);
   void changeTurn();


};
#endif
