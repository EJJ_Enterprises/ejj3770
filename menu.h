#ifndef MENU_H
#define MENU_H

#include <QtGui>
#include <QObject>
#include "game.h"

class menu : public QWidget {

Q_OBJECT
  public:
   QWidget* menuWindow;
	QWidget* winnerWindow;
   QCheckBox* forceJump;
  menu();

 private:
  game* checkersGame;
	QString qInstruction;
//	QString teamName;

  
  QVBoxLayout* top;
  QHBoxLayout* buttons;
  QHBoxLayout* options;
  QHBoxLayout* red;
  QHBoxLayout* black;
//  QHBoxLayout* winner;
//  QVBoxLayout* winnerButton
	//QHBoxLayout* winningTeam;

  QPushButton* exit;
  QPushButton* startGame;
  QPushButton* instructions;
//	QPushButton* replay;

  QLabel * title;
  QLabel * redWinsLabel;
  QLabel * blackWinsLabel;
  QLabel * numRedWins;
  QLabel * numBlackWins;
//	QLabel* winnerID;

  QDialog * instructionWindow;

  public slots:
  void createGame();

};

#endif
