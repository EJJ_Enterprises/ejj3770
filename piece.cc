#include "piece.h"

piece::piece(const color c, coord* startingSquare) : player { c }, location {startingSquare}
{
   redPiece = new QIcon("./Pictures/redPiece.jpg");
   redKing  = new QIcon("./Pictures/redKing.jpg");
   blackPiece = new QIcon("./Pictures/blackPiece.jpg");
   blackKing = new QIcon("./Pictures/blackKing.jpg");
   
  reset();
  if (player == piece::red)
    icon = redPiece;
  if (player == piece::black)
     icon = blackPiece;

  alive = 1;
}

void piece::reset(){
  alive = 1;
  kinged = 0;
}

void piece::setLocation(coord* newCoord){
  location = newCoord;
}

coord* piece::getLocation(){
  return location;
}

void piece::king(){
  kinged = 1;
  if (player==piece::red)
    setIcon(redKing);
  if (player==piece::black)
    setIcon(blackKing);
}

bool piece::isKinged(){
  return kinged;
}

void piece::kill(){
  alive = 0;
}

bool piece::isAlive(){
   return alive;
}

void piece::setIcon(QIcon* newIcon){
    icon = newIcon;
}

QIcon piece::getIcon(){
  return *icon;
}
