#ifndef GAME_H
#define GAME_H

#include <QtGui>
#include <vector>
#include "board.h"
#include "piece.h"
#include "coord.h"

class QWidget;

class game :public QWidget {
  Q_OBJECT

 public:
  game();
  QWidget* gameWindow;
  board* checkersBoard;

  public slots:
  void quitGame();

};

#endif
