#include <iostream>
#include "board.h"

board::board(){
   iSize = 75;
   moved=0;
   jumped=0;

   pieceJumped = false;
   movingPiece = NULL;
   infoSet = false;

   numRed = 12;
   numBlack = 12;

   turnLabel = new QLabel("<b><Font size = 5>Turn: Black");
   mainLayer = new QVBoxLayout();

   squareButton = new QButtonGroup; //All the  buttons for the 'board'
   checkersGrid = new QGridLayout;   //The gridlayout for the board

   pass = new QPushButton("Pass");
   pass -> setEnabled(0);
   connect(pass, SIGNAL(clicked()), this, SLOT(changeTurn()));
   
   QHBoxLayout * topLayer = new QHBoxLayout();
   
   topLayer -> addWidget(turnLabel);
   topLayer -> addWidget(pass);
   mainLayer -> addLayout(topLayer);
   mainLayer -> addLayout(checkersGrid);
   
   redSquare = new QIcon("./Pictures/redSquare.jpg");
   blackSquare = new QIcon("./Pictures/blackSquare.jpg");
   highlightedSquare = new QIcon("./Pictures/highlightedSquare.jpg");

   redPieceIcon = new QIcon("./Pictures/redPiece.jpg");
   blackPieceIcon = new QIcon ("./Pictures/blackPiece.jpg");
   
  //Eliminates the spacings between all buttons
  checkersGrid->setSpacing(0);

  //Will create all the buttons and their respective icons
  for (int r=0; r<8; r++){
    for (int c=0; c<8; c++){
      QSize size(iSize, iSize);
      QPushButton* button = new QPushButton;
      button->setMinimumWidth(iSize);
      button->setMaximumWidth(iSize);
      button->setMinimumHeight(iSize);
      button->setMaximumHeight(iSize);
      button->setIconSize(size);

      squareButton->addButton(button, r*10+c);
      checkersGrid->addWidget(squareButton->button(r*10+c), r, c);

      if ( (r+c) % 2) {
        squareButton->button(r*10+c)->setIcon(*blackSquare);
      }
      else {
        squareButton->button(r*10+c)->setIcon(*redSquare);
      }
    }
  }

  //Setup the buttons to show the paths by default
  QObject::connect(squareButton, SIGNAL(buttonClicked(int)), this, SLOT(getPath(int)));

  //Sets all the pieces on the board
  resetPieces();

  winnerWindow = new QDialog();
  
//  QPushButton * accept = new QPushButton("Sweet!");
  accept = new QPushButton("Sweet!");
  QVBoxLayout * dialogLayout = new QVBoxLayout();
  championLabel = new QLabel("Champion");
  dialogLayout -> addWidget(championLabel);
  dialogLayout -> addWidget(accept);
  winnerWindow -> setLayout(dialogLayout);

  QObject::connect(accept, SIGNAL(clicked()), winnerWindow, SLOT(close()));
}

void board::resetPieces(){

  //Erases the vectors holding the pieces
   // redPieces.clear();
   // blackPieces.clear();
   for(unsigned int i=0; i<redPieces.size(); i++)
   {
      delete redPieces[i];
      redPieces[i] = NULL;
   }
    for(unsigned int i=0; i<redPieces.size(); i++)
   {
      delete blackPieces[i];
      blackPieces[i] = NULL;
   }
   

  for (int r=0; r<3; r++){
    for (int c=0; c<8; c++){
      if ( (r+c)%2 ){
        int temp = r*10+c;

        redPieces.push_back(new piece(piece::red, new coord(r, c)));
        blackPieces.push_back(new piece(piece::black, new coord(7-r, 7-c)));

        squareButton->button(temp)->setIcon(*redPieceIcon);
        squareButton->button(77-temp)->setIcon(*blackPieceIcon);
      }
    }
  }
}

void board::highlightSquares(){

  //Highlight all squares that are possible movement destinations
  for (unsigned int i=0; i<jumpPath.size(); i++){
    squareButton->button(jumpPath[i]->getButtonID())->setIcon(*highlightedSquare);
  }

  for (unsigned int i=0; i<lastPath.size(); i++){
    squareButton->button(lastPath[i]->getButtonID())->setIcon(*highlightedSquare);
  }

  for (unsigned int i=0; i<testPath.size(); i++){
    squareButton->button(testPath[i]->getButtonID())->setIcon(*highlightedSquare);
  }
}

piece* board::getPiece(coord* location){
  for (int i=0; i<12; i++){
    if (location->getButtonID() == redPieces[i]->getLocation()->getButtonID() )
      if (redPieces[i]->isAlive())
        return redPieces[i];
    if (location->getButtonID() == blackPieces[i]->getLocation()->getButtonID() )
      if (blackPieces[i]->isAlive())
        return blackPieces[i];
  }

  return NULL;
}

bool board::jumpable(piece* one, piece* two){
  if ( one->player != two->player ){
    int r = one->getLocation()->r + ((two->getLocation()->r - one->getLocation()->r) * 2);
    int c = one->getLocation()->c + ((two->getLocation()->c - one->getLocation()->c) * 2);

    if (r<0 || c<0 || r>7 || c>7) //If coord is not valid
      return false;

    coord* tempCoord = new coord(r,c);
    
    if (getPiece(tempCoord)==NULL)
    {
       delete tempCoord;
       tempCoord = NULL;
       return true;
    }
  }
  return false;
}

void board::movePiece(piece* mover, coord* destination){

  //Sets where the pieces moves from to a black square icon
  squareButton->button(mover->getLocation()->getButtonID())->setIcon(*blackSquare);

  //Changes the coord of the piece that will be moved
  mover->setLocation(destination);

  //Places the piece icon on the new coord
  squareButton->button(destination->getButtonID())->setIcon(mover->getIcon());
  
  //Checks if the piece should be kinged and if so kings it
  if (mover->player==piece::red && mover->getLocation()->r==7){
    mover->king();
    squareButton->button(mover->getLocation()->getButtonID())->setIcon(mover->getIcon());
  }
  if (mover->player==piece::black && mover->getLocation()->r==0){
    mover->king();
    squareButton->button(mover->getLocation()->getButtonID())->setIcon(mover->getIcon());
  }

  movingPiece = mover;
  
  resetPaths();
 
  if (!pieceJumped)
    changeTurn();

  if (checkGameOver())
  {
     turnLabel->setText("<b><Font size = 5>GAME OVER!!");
  }
}

void board::jumpPiece(piece* jumper, coord* destination){

  int rDiff = (destination->r - jumper->getLocation()->r)/2;
  int cDiff = (destination->c - jumper->getLocation()->c)/2;

  coord* jumped = new coord(jumper->getLocation()->r+rDiff, jumper->getLocation()->c+cDiff);

  squareButton->button(jumped->getButtonID())->setIcon(*blackSquare);
  getPiece(jumped)->kill();

  pieceJumped = true;
  
  movePiece(jumper, destination);


  //Sets all icons back to black
  for (unsigned int i=0; i<testPath.size(); i++){
     if (getPiece(testPath[i])==NULL)
	 squareButton->button(testPath[i]->getButtonID())->setIcon(*blackSquare);
  }
  testPath.clear();
  //I used reset paths and stuff so that I wouldn't have to copy and paste large path algorithm
  resetPaths();
  pieceJumped=0;
  getPath(jumper->getLocation()->getButtonID());
  pieceJumped=1;
  testPath=jumpPath;
  resetPaths();
  infoSet=1;
  highlightSquares();
  pass->setEnabled(1);
  if (testPath.size()==0)
    changeTurn();

  if (checkGameOver())
  {
     turnLabel->setText("<b><Font size = 5>GAME OVER!!");
  }
}

void board::getPath(int buttonID){

  if (pieceJumped && infoSet){
      for (unsigned int p=0; p<testPath.size(); p++){
          if(testPath[p]->getButtonID()==buttonID){
              jumpPiece(lastClicked, testPath[p]);
              p=testPath.size();
              moved=1;
              jumped=1;
            }
        }
    }

  if(pieceJumped == false)
    {
      for (unsigned int i=0; i<jumpPath.size(); i++)
      {
	 if (jumpPath[i]->getButtonID() == buttonID)
	 {
	    jumpPiece(lastClicked, jumpPath[i]);
	    i=jumpPath.size();
	    jumped = 1;
	    moved=1;
	 }
      }
      for (unsigned int i=0; i<lastPath.size(); i++)
      {
	 if (lastPath[i]->getButtonID() == buttonID)
	 {
	    movePiece(lastClicked, lastPath[i]);
	    i = lastPath.size();
	    moved=1;
	 }
      }
      
      
      //Resets the paths
      resetPaths();
      
      //Sets last clicked button
      lastClicked = getPiece(new coord(buttonID/10, buttonID%10));
   
      //Checkes if piece exists on last clicked location
      if (lastClicked==NULL);
      else //last clicked is not NULL.
	 if (((lastClicked->player == 0) && (turnLabel->text() == "<b><Font size = 5>Turn: Black")) || ((lastClicked->player == 1) && (turnLabel-> text() == "<b><Font size = 5>Turn: Red"))) //checks if clicked piece matches turn of current player.
      {

	 //If piece exists on last clicked location create the vectors for the possible paths
	 if (!moved){
	    int r = lastClicked->getLocation()->r;
	    int c = lastClicked->getLocation()->c;
	    
	    //Movement 'up' the board
	    if (r-1<0); //If not valid row
	    else if (lastClicked->player==piece::black || lastClicked->isKinged()){
	       
	       //Movement left
	       coord* left = new coord(r-1, c-1);
 	       if (c-1<0); //Not valid col
	       else if (getPiece(left)==NULL){
		  lastPath.push_back(left);
	       }
	       else if (jumpable(lastClicked, getPiece(left))){
		  jumpPath.push_back(new coord(r-2, c-2));
	       }
	       
	       //Movement right
	       coord* right = new coord(r-1, c+1);
	       if (c+1>7); //Not valid col
	       else if (getPiece(right)==NULL){
		  lastPath.push_back(right);
	       }
	       else if (jumpable(lastClicked, getPiece(right))){
		  jumpPath.push_back(new coord(r-2, c+2));
	       }
	    }
	    
	    //Movement 'down' the board
	    if (r+1>7); //If not valid row
	    else if (lastClicked->player==piece::red || lastClicked->isKinged()){
	       
	       //Movement left
	       coord* left = new coord(r+1, c-1);
	       if (c-1<0); //Not valid col
	       else if (getPiece(left)==NULL){
		  lastPath.push_back(left);
	       }
	       else if (jumpable(lastClicked, getPiece(left))){
		  jumpPath.push_back(new coord(r+2, c-2));
	       }
	       
	       //Movement right
	       coord* right = new coord(r+1, c+1);
	       if (c+1>7); //Not valid col
	       else if (getPiece(right)==NULL){
		  lastPath.push_back(right);
	       }
	       else if (jumpable(lastClicked, getPiece(right))){
		  jumpPath.push_back(new coord(r+2, c+2));
	       }
	    }
	    highlightSquares();
	    
	 }
      }
   moved=0;  
   }

}

void board::resetPaths(){

  //Resets icons Back to Black
  for (unsigned int i=0; i<lastPath.size(); i++){
    if (getPiece(lastPath[i])==NULL)
      squareButton->button(lastPath[i]->getButtonID())->setIcon(*blackSquare);
  }
  for (unsigned int i=0; i<jumpPath.size(); i++){
    if (getPiece(jumpPath[i])==NULL)
      squareButton->button(jumpPath[i]->getButtonID())->setIcon(*blackSquare);
  }

  //clears the vectors
  lastPath.clear();
  jumpPath.clear();
}

void board::changeTurn()
{
  if (turnLabel->text() == "<b><Font size = 5>Turn: Black")
     turnLabel->setText("<b><Font size = 5>Turn: Red");
  
 else if (turnLabel->text() == "<b><Font size = 5>Turn: Red")
    turnLabel->setText("<b><Font size = 5>Turn: Black");

  for(unsigned int i=0; i<testPath.size(); i++)
  {
     squareButton->button(testPath[i]->getButtonID())->setIcon(*blackSquare);
  }
  
  pieceJumped = false;
  movingPiece = NULL;
  infoSet = false;
  resetPaths();
  testPath.clear();
  pass->setEnabled(0);


}

bool board::checkGameOver() //returns true if the game is over.
{
   piece * checkMoves;
   
   if(turnLabel->text() == "<b><Font size = 5>Turn: Red")
   {
      for(unsigned int i=0; i<redPieces.size(); i++)
      {
	 if(redPieces[i]->isAlive() == 1)
	 {
	    checkMoves = redPieces[i];
	    //check if the piece has any moves whatsoever.
	    
	    //If piece exists on last clicked location create the vectors for the possible paths
	    int r = checkMoves->getLocation()->r;
	    int c = checkMoves->getLocation()->c;
	    
	    //Movement 'up' the board
	    if (r-1<0)
	    {}//If not valid row, do nothing.
	    else if(checkMoves->isKinged() == true)
	    {
	       //Movement left
	       coord* left = new coord(r-1, c-1);
	       if (c-1<0)
	       {}//Not valid col, do nothing.
	       else
	       {
		  if (getPiece(left)==NULL)
		     return false;
		  if (jumpable(checkMoves, getPiece(left)))
		     return false;
	       }
	       //Movement right
	       coord* right = new coord(r-1, c+1);
	       if (c+1>7)
	       {}//Not valid col, do nothing.
	       else
	       {
		  if (getPiece(right)==NULL)
		     return false;
		  
		  if (jumpable(checkMoves, getPiece(right)))
		     return false;
	       }
	    }
	    //Movement 'down' the board
	    if (r+1>7)
	    {}//If not valid row, do nothing.
	    else 
	    {
	       //Movement left
	       coord* left = new coord(r+1, c-1);
	       if (c-1<0)
	       {}//Not valid col
	       else
	       {
		  if (getPiece(left)==NULL)
		     return false;
		  
		  if (jumpable(checkMoves, getPiece(left)))
		     return false;
	       }
	       
	       //Movement right
	       coord* right = new coord(r+1, c+1);
	       if (c+1>7){} //Not valid col
	       else
	       {
		  if (getPiece(right)==NULL)
		     return false;
		  
		  if (jumpable(checkMoves, getPiece(right)))
		     return false;
	       }
	    }
	 }
      }
      winnerWindow -> setWindowTitle("Black Wins!");
      championLabel -> setText("<b><Font size = 15>Black is the champion!");
   }
   else //turn is black.  
   {  
      for(unsigned int i=0; i<blackPieces.size(); i++)
      {
	 if(blackPieces[i]->isAlive() == 1)
	 {
	    checkMoves = blackPieces[i];
	    //check if the piece has any moves whatsoever.
	    
	    //If piece exists on last clicked location create the vectors for the possible paths
	    int r = checkMoves->getLocation()->r;
	    int c = checkMoves->getLocation()->c;
	    
	    //Movement 'up' the board
	    if (r-1<0)
	    {}//If not valid row, do nothing.
	    else
	    {
	       //Movement left
	       coord* left = new coord(r-1, c-1);
	       if (c-1<0)
	       {}//Not valid col, do nothing.
	       else
	       {
		  if (getPiece(left)==NULL)
		     return false;
		  if (jumpable(checkMoves, getPiece(left)))
		     return false;
	       }
	    }
	    //Movement right
	    coord* right = new coord(r-1, c+1);
	    if (c+1>7)
	    {}//Not valid col, do nothing.
	    else
	    {
	       if (getPiece(right)==NULL)
		  return false;
	       
	       if (jumpable(checkMoves, getPiece(right)))
		  return false;
	    }
	    
	    //Movement 'down' the board
	    if (r+1>7)
	    {}//If not valid row, do nothing.
	    else if(checkMoves->isKinged() == true)
	    {
	       //Movement left
	       coord* left = new coord(r+1, c-1);
	       if (c-1<0)
	       {}//Not valid col
	       else
	       {
		  if (getPiece(left)==NULL)
		     return false;
		  
		  if (jumpable(checkMoves, getPiece(left)))
		     return false;
	       }
	       
	       //Movement right
	       coord* right = new coord(r+1, c+1);
	       if (c+1>7){} //Not valid col
	       else
	       {
		  if (getPiece(right)==NULL)
		     return false;
		  
		  if (jumpable(checkMoves, getPiece(right)))
		     return false;
	       }
	    }
	 }
      }
      winnerWindow -> setWindowTitle("Red Wins!");
      championLabel -> setText("<b><Font size = 15>Red is the champion!");
   }
   winnerWindow -> show();
   return true;
}
   
