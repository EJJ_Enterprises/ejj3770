#include "menu.h"
#include "game.h"
#include <iostream>
#include <fstream>
#include <string>
#include <QtGui>

menu::menu(){
   // checkersGame = new game();

std::ifstream instructionFile;
std::string gameInstruction;
std::string line;

instructionFile.open("instructions.txt");
if(instructionFile.is_open())
{
	while(getline(instructionFile, line))
	{
		gameInstruction += line;
		gameInstruction += '\n';
	}
	instructionFile.close();
}

qInstruction = QString::fromStdString(gameInstruction);
   
   menuWindow = new QWidget;
   menuWindow ->setWindowTitle("Checkers - Main Menu");
   forceJump = new QCheckBox ("Force Jump");
   
   checkersGame = NULL;
   
   top = new QVBoxLayout;
   buttons = new QHBoxLayout;
   options = new QHBoxLayout;
   red = new QHBoxLayout;
   black = new QHBoxLayout;
   
   exit = new QPushButton("Exit");
   startGame = new QPushButton("Play Game");
   instructions = new QPushButton("Instructions");

   redWinsLabel = new QLabel("# Red Wins: ");
   numRedWins = new QLabel();
//set number of red wins here.
   red->addWidget(redWinsLabel);
   red->addWidget(numRedWins);
   
   blackWinsLabel = new QLabel("# Black Wins: ");
   numBlackWins = new QLabel();
//set number of black wins here.
   black->addWidget(blackWinsLabel);
   black->addWidget(numBlackWins);

   QString titleString = "<b><Font size = 20>";
   titleString.append("Checkers: The Game of Heroes");
   title = new QLabel();
   title -> setText(titleString);
   
  //Adds widgets to layout
  options->addWidget(forceJump);
  buttons->addWidget(startGame);
  buttons->addWidget(instructions);
  buttons->addWidget(exit);

  //Sets layouts onto one layout for window
  top->addWidget(title);
  // top->addLayout(red);
  //top->addLayout(black);
  //top->addLayout(options);
  top->addLayout(buttons);

  //Sets main windows layout
  menuWindow->setLayout(top);

  instructionWindow = new QDialog();
  instructionWindow -> setWindowTitle("Instructions");
  QVBoxLayout * windowLayout = new QVBoxLayout();
  instructionWindow -> setLayout(windowLayout);
  QPushButton * closeWindow = new QPushButton("Close");
  QLabel * instructionList = new QLabel(qInstruction);
  windowLayout -> addWidget(instructionList);
  windowLayout -> addWidget(closeWindow);
  QObject::connect(closeWindow, SIGNAL(clicked()), instructionWindow, SLOT(close()));
/*
	//Winner Window
	winnerWindow = new QWidget;
	winner = new QVBoxLayout;
	winnerButton = new QHBoxLayout;
	winningTeam = new QHBoxLayout;
	//Reuse Exit Button.
	replay = new QPushButton("Play Again");
	//TODO Function to set teamName QString vvvvv
	teamName = new QString;
	winnerID = new QLabel(" is the Winner!");


	winningTeam->addWidget(teamName);
	winningTeam->addWidget(winnderID);
	winnerButton->addWidget(replay);
	winnerButton->addWidget(exit);	
	
	winner->addLayout(winningTeam);
	winner->addLayout(winnerButton);

	winnerWindow->addLayout(winner);

//Have not tested this so I commented it all out.
  
*/
  //Connect Buttons
  QObject::connect(exit, SIGNAL(clicked()), menuWindow, SLOT(close()));
  QObject::connect(startGame, SIGNAL(clicked()), this, SLOT(createGame()));
  QObject::connect(instructions, SIGNAL(clicked()), instructionWindow, SLOT(show()));
//TODO not sure how to connect a "replay buttong"
//	QObject::connect(replay, SIGNAL(clicked()), checkersGame->gameWindow, SLOT(show()));
}

void menu::createGame()
{
   if (checkersGame != NULL) 
   {
      delete checkersGame;
   }
   checkersGame = new game();
}
