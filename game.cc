#include "game.h"
#include "coord.h"

game::game(){
   gameWindow = new QWidget;
   gameWindow -> setWindowTitle("Checkers: The Game of Heroes");

   //Creates the board
   checkersBoard = new board();
 
  QPushButton * quitB = new QPushButton("Quit Game");
  QVBoxLayout * mainLayer = new QVBoxLayout();
  
  mainLayer -> addLayout(checkersBoard->mainLayer);
  mainLayer -> addWidget(quitB);
    
  //Set the board layout to the game window
  gameWindow->setLayout(mainLayer);

  gameWindow->show();
  
  connect(quitB, SIGNAL(clicked()), this, SLOT(quitGame()));
//Connect Test for SWEET!
  connect(checkersBoard->accept, SIGNAL(clicked()), this, SLOT(quitGame()));
}

void game::quitGame()
{
   delete checkersBoard;
   gameWindow->close();
   delete gameWindow;
}
