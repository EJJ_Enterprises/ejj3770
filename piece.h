#ifndef PIECE_H
#define PIECE_H

#include <vector>
#include <QtGui>
#include "coord.h"

class coord;
class piece {

 public:
  enum color {black, red};
  const color player;

  QIcon* redPiece;
  QIcon* redKing;
  QIcon* blackPiece;
  QIcon* blackKing;

  piece(const color c, coord* startingSquare);

  void reset();

  coord* getLocation();
  void setLocation(coord* newCoord);

  void king();
  bool isKinged();

  void kill();
  bool isAlive();

  void setIcon(QIcon* newIcon);
  QIcon getIcon();

 private:
  QIcon* icon;
  bool alive;
  bool kinged;
  coord* location;

};

#endif
